extends ColorRect

tool
export var make_unique: bool setget set_make_unique

func set_make_unique(in_value):
	$x1.material = $x1.material.duplicate()
	$x2flat.material = $x2flat.material.duplicate()
	$x2shade.material = $x2shade.material.duplicate()
	$x2bar.material = $x2bar.material.duplicate()
	$x3flat.material = $x3flat.material.duplicate()
	$x3shade.material = $x3shade.material.duplicate()
	$x3bar.material = $x3bar.material.duplicate()
	
export var shades: int = 256 setget set_shades
export var grayscale: float = 0.0 setget set_grayscale
export var cookie: Texture setget set_cookie

func set_shades(new_shades):
	shades = new_shades
	$x1.material.set_shader_param("bits", shades)
	$x2flat.material.set_shader_param("bits", shades)
	$x2shade.material.set_shader_param("bits", shades)
	$x2bar.material.set_shader_param("bits", shades)
	$x3flat.material.set_shader_param("bits", shades)
	$x3shade.material.set_shader_param("bits", shades)
	$x3bar.material.set_shader_param("bits", shades)

func set_cookie(new_cookie):
	cookie = new_cookie
	$x1.material.set_shader_param("cookie", cookie)
	$x2flat.material.set_shader_param("cookie", cookie)
	$x2shade.material.set_shader_param("cookie", cookie)
	$x2bar.material.set_shader_param("cookie", cookie)
	$x3flat.material.set_shader_param("cookie", cookie)
	$x3shade.material.set_shader_param("cookie", cookie)
	$x3bar.material.set_shader_param("cookie", cookie)

func set_grayscale(new_grayscale):
	grayscale = new_grayscale
	$x1.material.set_shader_param("grayscale", grayscale)
	$x2flat.material.set_shader_param("grayscale", grayscale)
	$x2shade.material.set_shader_param("grayscale", grayscale)
	$x2bar.material.set_shader_param("grayscale", grayscale)
	$x3flat.material.set_shader_param("grayscale", grayscale)
	$x3shade.material.set_shader_param("grayscale", grayscale)
	$x3bar.material.set_shader_param("grayscale", grayscale)
