extends Node2D


func _input(event):
	var key_event = event as InputEventKey
	if key_event && key_event.pressed:
		if key_event.scancode == KEY_KP_ADD:
			scale *= 1.1
			pass
		if key_event.scancode == KEY_KP_SUBTRACT:
			scale /= 1.1
			pass
