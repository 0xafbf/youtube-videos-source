extends Node

func _input(event):
	var key_event = event as InputEventKey
	if key_event && key_event.pressed && key_event.scancode == KEY_F11:
		OS.window_fullscreen = !OS.window_fullscreen
