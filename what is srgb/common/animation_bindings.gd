extends AnimationPlayer

func _input(event):
	var key_event = event as InputEventKey
	if key_event && key_event.pressed:
		if key_event.scancode == KEY_S:
			stop()
		if key_event.scancode == KEY_P:
			play()
