extends Node2D

export var speed = Vector2(1000, 1000)

func _process(delta: float):
	var vertical = Input.get_action_strength("ui_up") -Input.get_action_strength("ui_down")	
	var horizontal = Input.get_action_strength("ui_right") -Input.get_action_strength("ui_left")	
	position += Vector2(horizontal, -vertical) * delta * speed
